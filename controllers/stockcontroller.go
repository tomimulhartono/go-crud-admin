package controllers

import (
	"net/http"
	"strconv"
	"text/template"

	"gitlab.com/tomimulhartono/go-crud-admin/entities"
	"gitlab.com/tomimulhartono/go-crud-admin/libraries"
	"gitlab.com/tomimulhartono/go-crud-admin/models"
)

var validation = libraries.NewValidation()
var stockModel = models.NewStockModel()

func Index(response http.ResponseWriter, request *http.Request) {

	stock, _ := stockModel.FindAll()

	data := map[string]interface{}{
		"stock": stock,
	}

	temp, err := template.ParseFiles("views/index.html")
	if err != nil {
		panic(err)
	}
	temp.Execute(response, data)
}

func Add(response http.ResponseWriter, request *http.Request) {

	if request.Method == http.MethodGet {
		temp, err := template.ParseFiles("views/add.html")
		if err != nil {
			panic(err)
		}
		temp.Execute(response, nil)

	} else if request.Method == http.MethodPost {

		request.ParseForm()

		var stock entities.Stock
		stock.Code = request.Form.Get("code")
		stock.Name = request.Form.Get("name")
		stock.Total = request.Form.Get("total")
		stock.Description = request.Form.Get("description")
		stock.Status = request.Form.Get("status")

		var data = make(map[string]interface{})

		vErrors := validation.Struct(stock)

		if vErrors != nil {
			data["stock"] = stock
			data["validation"] = vErrors
		} else {
			data["pesan"] = "Data barang berhasil disimpan"
			stockModel.Create(stock)
		}

		temp, _ := template.ParseFiles("views/add.html")
		temp.Execute(response, data)
	}

}

func Edit(response http.ResponseWriter, request *http.Request) {

	if request.Method == http.MethodGet {

		queryString := request.URL.Query()
		id, _ := strconv.ParseInt(queryString.Get("id"), 10, 64)

		var stock entities.Stock
		stockModel.Find(id, &stock)

		data := map[string]interface{}{
			"stock": stock,
		}

		temp, err := template.ParseFiles("views/edit.html")
		if err != nil {
			panic(err)
		}
		temp.Execute(response, data)

	} else if request.Method == http.MethodPost {

		request.ParseForm()

		var stock entities.Stock
		stock.Id, _ = strconv.ParseInt(request.Form.Get("id"), 10, 64)
		stock.Code = request.Form.Get("code")
		stock.Name = request.Form.Get("name")
		stock.Total = request.Form.Get("total")
		stock.Description = request.Form.Get("description")
		stock.Status = request.Form.Get("status")

		var data = make(map[string]interface{})

		vErrors := validation.Struct(stock)

		if vErrors != nil {
			data["stock"] = stock
			data["validation"] = vErrors
		} else {
			data["pesan"] = "Data barang berhasil diperbarui"
			stockModel.Update(stock)
		}

		temp, _ := template.ParseFiles("views/edit.html")
		temp.Execute(response, data)
	}

}

func Delete(response http.ResponseWriter, request *http.Request) {

	queryString := request.URL.Query()
	id, _ := strconv.ParseInt(queryString.Get("id"), 10, 64)

	stockModel.Delete(id)

	http.Redirect(response, request, "/stock", http.StatusSeeOther)
}
