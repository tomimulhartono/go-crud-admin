package entities

type Stock struct {
	Id          int64
	Code        string `validate:"required" label:"Code"`
	Name        string `validate:"required" label:"Name"`
	Total       string `validate:"required" label:"Total"`
	Description string `validate:"required" label:"Description"`
	Status      string `validate:"required" label:"Status"`
}
