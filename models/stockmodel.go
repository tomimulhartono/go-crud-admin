package models

import (
	"database/sql"
	"fmt"

	"gitlab.com/tomimulhartono/go-crud-admin/config"
	"gitlab.com/tomimulhartono/go-crud-admin/entities"
)

type StockModel struct {
	conn *sql.DB
}

func NewStockModel() *StockModel {
	conn, err := config.DBConnection()
	if err != nil {
		panic(err)
	}

	return &StockModel{
		conn: conn,
	}
}

func (p *StockModel) FindAll() ([]entities.Stock, error) {

	rows, err := p.conn.Query("select * from stock")
	if err != nil {
		return []entities.Stock{}, err
	}
	defer rows.Close()

	var dataStock []entities.Stock
	for rows.Next() {
		var stock entities.Stock
		rows.Scan(&stock.Id,
			&stock.Code,
			&stock.Name,
			&stock.Total,
			&stock.Description,
			&stock.Status)

		if stock.Status == "1" {
			stock.Status = "Active"
		} else {
			stock.Status = "Not Active"
		}

		dataStock = append(dataStock, stock)
	}

	return dataStock, nil

}

func (p *StockModel) Create(stock entities.Stock) bool {

	result, err := p.conn.Exec("insert into stock (code, name, total, description, status) values (?,?,?,?,?)",
		stock.Code, stock.Name, stock.Total, stock.Description, stock.Status)

	if err != nil {
		fmt.Println(err)
		return false
	}

	lastInsertId, _ := result.LastInsertId()

	return lastInsertId > 0
}

func (p *StockModel) Find(id int64, stock *entities.Stock) error {

	return p.conn.QueryRow("select * from stock where id = ?", id).Scan(
		&stock.Id,
		&stock.Code,
		&stock.Name,
		&stock.Total,
		&stock.Description,
		&stock.Status)
}

func (p *StockModel) Update(stock entities.Stock) error {

	_, err := p.conn.Exec(
		"update stock set code = ?, name = ?, total = ?, description = ?, status = ? where id = ?",
		stock.Code, stock.Name, stock.Total, stock.Description, stock.Status, stock.Id)

	if err != nil {
		return err
	}

	return nil
}

func (p *StockModel) Delete(id int64) {
	p.conn.Exec("delete from stock where id = ?", id)
}
