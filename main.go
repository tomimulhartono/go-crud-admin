package main

import (
	"net/http"

	// "gitlab.com/tomimulhartono/go-crud-admin/controllers/stockcontroller"
	"gitlab.com/tomimulhartono/go-crud-admin/controllers/stockcontroller"
)

func main() {

	http.HandleFunc("/", stockcontroller.Index)
	http.HandleFunc("/stock", stockcontroller.Index)
	http.HandleFunc("/stock/index", stockcontroller.Index)
	http.HandleFunc("/stock/add", stockcontroller.Add)
	http.HandleFunc("/stock/edit", stockcontroller.Edit)
	http.HandleFunc("/stock/delete", stockcontroller.Delete)

	http.ListenAndServe(":3000", nil)
}
